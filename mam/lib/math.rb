module Math
  def self.max(a, b)
    a > b ? a : b
  end

  def self.min(a, b)
    a < b ? a : b
  end

  def self.abs(a)
    a > 0 ? a : -a
  end
end
