module Names 
  
  def self.getName
    name = getFirstKonsonant + getVocal
    
    name +=  getKonsonant + getVocal if 0 == rand(2)
    
    name +=  getKonsonant + getVocal if 0 == rand(2)
    
    name =  getVocal + name if 0 == rand(3)
    
    name =  name + getKonsonant if 0 == rand(3)
    
    if 2 == name.length
      if 0 == rand(1) 
        name =  getVocal + name
      else
        name =  name + getKonsonant
      end
    end
    
    name.capitalize
  end
  
  def self.getVocal
    ['a', 'e', 'i', 'o', 'u'].sort_by{rand}[0]
  end
  
  def self.getFirstKonsonant
    ['b', 'c', 'd', 'f', 'g', 'h', 'k', 'l', 'm', 'n', 'p', 'r', 's', 't', 'v',
     'w', 'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r',
     's', 't', 'v', 'w', 'x', 'y', 'z'].sort_by{rand}[0]
  end
  
  def self.getKonsonant
    ['b', 'c', 'd', 'f', 'g', 'h', 'k', 'l', 'm', 'n', 'p', 'r', 's', 't', 'v',
     'w', 'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r',
     's', 't', 'v', 'w', 'x', 'y', 'z', 'b', 'ck', 'll', 'mm', 'nn', 'pp',
     'sh', 'tt'].sort_by{rand}[0]
  end 
end
