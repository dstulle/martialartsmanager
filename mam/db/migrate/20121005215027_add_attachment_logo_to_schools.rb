class AddAttachmentLogoToSchools < ActiveRecord::Migration
  def self.up
    change_table :schools do |t|
      t.has_attached_file :logo
    end
  end

  def self.down
    drop_attached_file :schools, :logo
  end
end
