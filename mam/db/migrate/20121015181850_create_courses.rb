class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string :name
      t.references :school
      t.integer :teacher_id
      t.integer :train_strength
      t.integer :train_constitution
      t.integer :train_dexterity
      t.integer :train_speed
      t.integer :train_flexibility
      t.integer :train_reaction
      t.integer :train_skill
      t.integer :train_intelligence

      t.timestamps
    end
    add_index :courses, :school_id
  end
end
