class CreateBuddiesAttenfingCourses < ActiveRecord::Migration
  def change
    create_table :buddies_attending_courses, :id => false do |t|
      t.column "buddy_id", :integer
      t.column "course_id", :integer
    end
  end
end
