class CreateBuddies < ActiveRecord::Migration
  def change
    create_table :buddies do |t|
      t.string :name
      t.integer :age
      t.references :school
      t.boolean :waitlist
      t.integer :strength_exp
      t.integer :constitution_exp
      t.integer :dexterity_exp
      t.integer :speed_exp
      t.integer :flexibility_exp
      t.integer :reaction_exp
      t.integer :skill_exp
      t.integer :intelligence_exp
      t.integer :suitability
      t.integer :motivation
      t.integer :injured
      t.integer :number_of_fights
      t.integer :number_of_wins
      t.integer :number_of_draws

      t.timestamps
    end
  end
end
