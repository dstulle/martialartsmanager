class CreateBuddiesExmembersOfSchoolTable < ActiveRecord::Migration
  def up
    create_table :buddies_exmembers_of_school, :id => false do |t|
      t.references :buddy
      t.references :school
    end
    add_index :buddies_exmembers_of_school, [:buddy_id, :school_id]
    add_index :buddies_exmembers_of_school, [:school_id, :buddy_id]
  end

  def down
    drop_table :buddies_exmembers_of_school
  end
end
