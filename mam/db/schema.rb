# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121227191251) do

  create_table "buddies", :force => true do |t|
    t.string   "name"
    t.integer  "age"
    t.integer  "school_id"
    t.boolean  "waitlist"
    t.integer  "strength_exp"
    t.integer  "constitution_exp"
    t.integer  "dexterity_exp"
    t.integer  "speed_exp"
    t.integer  "flexibility_exp"
    t.integer  "reaction_exp"
    t.integer  "skill_exp"
    t.integer  "intelligence_exp"
    t.integer  "suitability"
    t.integer  "motivation"
    t.integer  "injured"
    t.integer  "number_of_fights"
    t.integer  "number_of_wins"
    t.integer  "number_of_draws"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "buddies_attending_courses", :id => false, :force => true do |t|
    t.integer "buddy_id"
    t.integer "course_id"
  end

  create_table "buddies_exmembers_of_school", :id => false, :force => true do |t|
    t.integer "buddy_id"
    t.integer "school_id"
  end

  add_index "buddies_exmembers_of_school", ["buddy_id", "school_id"], :name => "index_buddies_exmembers_of_school_on_buddy_id_and_school_id"
  add_index "buddies_exmembers_of_school", ["school_id", "buddy_id"], :name => "index_buddies_exmembers_of_school_on_school_id_and_buddy_id"

  create_table "courses", :force => true do |t|
    t.string   "name"
    t.integer  "school_id"
    t.integer  "teacher_id"
    t.integer  "train_strength"
    t.integer  "train_constitution"
    t.integer  "train_dexterity"
    t.integer  "train_speed"
    t.integer  "train_flexibility"
    t.integer  "train_reaction"
    t.integer  "train_skill"
    t.integer  "train_intelligence"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "courses", ["school_id"], :name => "index_courses_on_school_id"

  create_table "players", :force => true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  create_table "schools", :force => true do |t|
    t.string   "name"
    t.integer  "player_id"
    t.text     "description"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "username"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
