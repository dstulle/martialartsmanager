require 'test_helper'

class BuddiesControllerTest < ActionController::TestCase
  setup do
    @buddy = buddies(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:buddies)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create buddy" do
    assert_difference('Buddy.count') do
      post :create, buddy: { age: @buddy.age, constitution_exp: @buddy.constitution_exp, dexterity_exp: @buddy.dexterity_exp, flexibility_exp: @buddy.flexibility_exp, injured_month: @buddy.injured_month, intelligence_exp: @buddy.intelligence_exp, motivation: @buddy.motivation, name: @buddy.name, number_of_draws: @buddy.number_of_draws, number_of_fights: @buddy.number_of_fights, number_of_wins: @buddy.number_of_wins, reaction_exp: @buddy.reaction_exp, skill_exp: @buddy.skill_exp, speed_exp: @buddy.speed_exp, strength_exp: @buddy.strength_exp, suitability: @buddy.suitability, waiting_for_school_id: @buddy.waiting_for_school_id }
    end

    assert_redirected_to buddy_path(assigns(:buddy))
  end

  test "should show buddy" do
    get :show, id: @buddy
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @buddy
    assert_response :success
  end

  test "should update buddy" do
    put :update, id: @buddy, buddy: { age: @buddy.age, constitution_exp: @buddy.constitution_exp, dexterity_exp: @buddy.dexterity_exp, flexibility_exp: @buddy.flexibility_exp, injured_month: @buddy.injured_month, intelligence_exp: @buddy.intelligence_exp, motivation: @buddy.motivation, name: @buddy.name, number_of_draws: @buddy.number_of_draws, number_of_fights: @buddy.number_of_fights, number_of_wins: @buddy.number_of_wins, reaction_exp: @buddy.reaction_exp, skill_exp: @buddy.skill_exp, speed_exp: @buddy.speed_exp, strength_exp: @buddy.strength_exp, suitability: @buddy.suitability, waiting_for_school_id: @buddy.waiting_for_school_id }
    assert_redirected_to buddy_path(assigns(:buddy))
  end

  test "should destroy buddy" do
    assert_difference('Buddy.count', -1) do
      delete :destroy, id: @buddy
    end

    assert_redirected_to buddies_path
  end
end
