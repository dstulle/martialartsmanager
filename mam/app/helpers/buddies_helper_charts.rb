require 'gruff'
require 'RMagick'

include Magick

module BuddiesHelper

  class Gruff::Base
    
    def theme_mam_attributes
      reset_themes
      @colors = "d555d555ddd5d5d5dd555ddd".scan(/.../).collect {|c| "##{c}"}
      @font_color = 'black'
      @marker_color = 'black'
      @base_image = render_gradiated_background('#fff', '#fff')
    end
    
    def theme_mam_attributes_thumb
      reset_themes
      @colors = "d555d555ddd5d5d5dd555ddd".scan(/.../).collect {|c| "##{c}"}
      @font_color = 'black'
      @marker_color = 'black'
      @base_image = render_transparent_background
    end
    
    def theme_mam_motivation
      reset_themes
      @colors = "950d90".scan(/.../).collect {|c| "##{c}"}
      @font_color = 'black'
      @marker_color = '#000'
      @base_image = render_gradiated_background('#fff', 'white')
    end
    
    def theme_mam_motivation_thumb
      reset_themes
      @colors = "950d90".scan(/.../).collect {|c| "##{c}"}
      @font_color = 'black'
      @marker_color = '#000'
      @base_image = render_transparent_background
    end
    
  end
  
  def attribute_legend_url
    'tmp/legend.png'
  end
  
  def attribute_bar_chart_url(buddy)
    
    imagename = '/images/tmp/'
    imagename << 'str' + buddy.strength.to_s + "_"
    imagename << 'con' + buddy.constitution.to_s + "_"
    imagename << 'dex' + buddy.dexterity.to_s + "_"
    imagename << 'spd' + buddy.speed.to_s + "_"
    imagename << 'flx' + buddy.flexibility.to_s + "_"
    imagename << 'rea' + buddy.reaction.to_s + "_"
    imagename << 'skl' + buddy.skill.to_s + "_"
    imagename << 'int' + buddy.intelligence.to_s + "_"
    imagename << 'bar.png'
    
    if not File.exists?("public#{imagename}")
      g = Gruff::Bar.new('300x200')
      g.theme_mam_attributes

      g.hide_legend = true;
      
      g.maximum_value = 20
      g.minimum_value = 0
      g.marker_count = 5
      g.sort = false
      
      g.data('Strength', buddy.strength)
      g.data('Constitution', buddy.constitution)
      g.data('Dexterity', buddy.dexterity)
      g.data('Speed', buddy.speed)
      g.data('Flexibility', buddy.flexibility)
      g.data('Reaction', buddy.reaction)
      g.data('Skill', buddy.skill)
      g.data('Intelligence', buddy.intelligence)
      
      g.write("public#{imagename}")
      
    end
    
    imagename
  end
  
  def attribute_bar_thumb_url(buddy)
    
    imagename = '/images/tmp/'
    imagename << 'str' + buddy.strength.to_s + "_"
    imagename << 'con' + buddy.constitution.to_s + "_"
    imagename << 'dex' + buddy.dexterity.to_s + "_"
    imagename << 'spd' + buddy.speed.to_s + "_"
    imagename << 'flx' + buddy.flexibility.to_s + "_"
    imagename << 'rea' + buddy.reaction.to_s + "_"
    imagename << 'skl' + buddy.skill.to_s + "_"
    imagename << 'int' + buddy.intelligence.to_s + "_"
    imagename << 'thumb_bar.png'
    
    if true or not File.exists?("public#{imagename}")
      g = Gruff::Bar.new('100x50')
      g.theme_mam_attributes_thumb
      
      g.hide_legend = true;
      g.hide_line_markers = true
      g.hide_line_numbers = true;
      g.hide_title = true;
      
      g.maximum_value = 24
      g.minimum_value = 0
      g.marker_count = 6
      g.sort = false
      
      g.data('Strength', buddy.strength)
      g.data('Constitution', buddy.constitution)
      g.data('Dexterity', buddy.dexterity)
      g.data('Speed', buddy.speed)
      g.data('Flexibility', buddy.flexibility)
      g.data('Reaction', buddy.reaction)
      g.data('Skill', buddy.skill)
      g.data('Intelligence', buddy.intelligence)
      
      g.write("public#{imagename}")
      
    end
    
    imagename
  end
  
  def motivation_bar_chart_url(buddy)
    
    imagename = '/images/tmp/'
    imagename << 'suit' + buddy.suitability.to_s + "_"
    imagename << 'moti' + buddy.motivation.to_s + "_"
    
    imagename << 'bar.png'
    
    if not File.exists?("public#{imagename}")
      g = Gruff::Bar.new('60x300')
      g.theme_mam_motivation
      g.hide_legend = true;
      g.hide_line_numbers = true;
      
      g.maximum_value = 1
      g.minimum_value = -1
      g.marker_count = 2
      g.sort = false
      
      g.data('Suit', buddy.suitability.to_f/100)
      g.data('Moti', buddy.motivation.to_f/100)
      
      g.write("public#{imagename}")
      
      img = Magick::Image.read("public#{imagename}").first
      new = img.rotate(90)
      new.write("public#{imagename}")
    end
    
    imagename
  end
  
  def motivation_bar_thumb_url(buddy)
    
    imagename = '/images/tmp/'
    imagename << 'suit' + buddy.suitability.to_s + "_"
    imagename << 'moti' + buddy.motivation.to_s + "_"
    
    imagename << 'thumb_bar.png'
    
    if not File.exists?("public#{imagename}")
      g = Gruff::Bar.new('30x65')
      g.theme_mam_motivation_thumb
      g.hide_legend = true;
      g.hide_line_markers = true;
      g.hide_line_numbers = true;
      
      g.maximum_value = 1.001
      g.minimum_value = -1
      g.marker_count = 2
      g.sort = false
      
      g.data('Suit', buddy.suitability.to_f/100)
      g.data('Moti', buddy.motivation.to_f/100)
      
      g.write("public#{imagename}")
    end
    
    imagename
  end
  
end
