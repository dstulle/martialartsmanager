require 'gruff'
require 'RMagick'

include Magick

module CoursesHelper

  class Gruff::Base
    
    def theme_mam_attributes
      reset_themes
      @colors = "d666d666ddd6d6d6dd666ddd".scan(/.../).collect {|c| "##{c}"}
      @font_color = 'black'
      @marker_color = 'black'
      @base_image = render_gradiated_background('#fff', '#fff')
    end
    
    def theme_mam_attributes_thumb
      reset_themes
      @colors = "d666d666ddd6d6d6dd666ddd".scan(/.../).collect {|c| "##{c}"}
      @font_color = 'black'
      @marker_color = 'black'
      @base_image = render_transparent_background
    end
    
  end
  
  def attribute_legend_url
    'tmp/legend.png'
  end
  
  def attribute_pie_chart_url(course)
    
    imagename = '/images/tmp/'
    imagename << 'str' + course.strength.to_s + "_"
    imagename << 'con' + course.constitution.to_s + "_"
    imagename << 'dex' + course.dexterity.to_s + "_"
    imagename << 'spd' + course.speed.to_s + "_"
    imagename << 'flx' + course.flexibility.to_s + "_"
    imagename << 'rea' + course.reaction.to_s + "_"
    imagename << 'skl' + course.skill.to_s + "_"
    imagename << 'int' + course.intelligence.to_s + "_"
    imagename << 'pie.png'
    
    if not File.exists?("public#{imagename}")
      g = Gruff::Pie.new('290x200')
      g.theme_mam_attributes
      g.zero_degree = -90
      
      g.hide_legend = true;
      g.hide_line_markers = false;
      g.hide_line_numbers = false;
      
      g.sort = false
      
      g.data('Strength', course.strength)
      g.data('Constitution', course.constitution)
      g.data('Dexterity', course.dexterity)
      g.data('Speed', course.speed)
      g.data('Flexibility', course.flexibility)
      g.data('Reaction', course.reaction)
      g.data('Skill', course.skill)
      g.data('Intelligence', course.intelligence)
      
      g.write("public#{imagename}")
      
    end
    
    imagename
  end
  
  def attribute_pie_thumb_url(course)
    
    imagename = '/images/tmp/'
    imagename << 'str' + course.strength.to_s + "_"
    imagename << 'con' + course.constitution.to_s + "_"
    imagename << 'dex' + course.dexterity.to_s + "_"
    imagename << 'spd' + course.speed.to_s + "_"
    imagename << 'flx' + course.flexibility.to_s + "_"
    imagename << 'rea' + course.reaction.to_s + "_"
    imagename << 'skl' + course.skill.to_s + "_"
    imagename << 'int' + course.intelligence.to_s + "_"
    imagename << 'thumb_pie.png'
    
#    if not File.exists?("public/images/#{imagename}")
      g = Gruff::Pie.new('65x65')
      g.theme_mam_attributes_thumb
      g.zero_degree = -90
      
      g.hide_legend = true;
      g.hide_line_markers = true;
      g.hide_line_numbers = true;

      
      g.sort = false
      
      g.data('Strength', course.strength)
      g.data('Constitution', course.constitution)
      g.data('Dexterity', course.dexterity)
      g.data('Speed', course.speed)
      g.data('Flexibility', course.flexibility)
      g.data('Reaction', course.reaction)
      g.data('Skill', course.skill)
      g.data('Intelligence', course.intelligence)
      
      g.write("public/images/#{imagename}")
      
#    end
    
    imagename
  end

end
