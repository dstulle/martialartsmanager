require 'buddies_helper_charts'

module BuddiesHelper
  
  def buddy_image(buddy, geometry="60x100")
    require "schools_helper"
    
    school_id = 0
    school_id = buddy.attending_school_id if buddy.attending_school_id != nil
    imagename = "/images/tmp/buddy_#{school_id}_#{buddy.belt_color}.png" 
    #if not File.exists?("public/images/#{imagename}")
    image = ImageList.new("gfx/buddy/background.png")
    belt = ImageList.new("public/images/belts/#{buddy.belt_color}_belt.png")
    image.composite!(belt,13,56,OverCompositeOp)
    if buddy.attending_school
      school_logo = ImageList.new("public/#{school_logo_image(buddy.attending_school, "thumb")}")
      school_logo.change_geometry!("14x14") { |cols, rows, img| img.resize!(cols, rows)}
      logo_x = 37-school_logo.columns/2
      logo_y = 47-school_logo.rows/2
      image.composite!(school_logo,logo_x,logo_y,OverCompositeOp)
    end
    image.change_geometry!(geometry) { |cols, rows, img| img.resize!(cols, rows)}
    image.write("public#{imagename}")
    #end
    imagename
  end

end
