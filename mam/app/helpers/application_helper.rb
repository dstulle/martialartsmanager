module ApplicationHelper

  def menu_item(*args)
    if args.length == 1
      url = args[0]
      body = url.to_s
    else
      body = args[0]
      url = args[1]
    end
    raw "<li#{class_active_controller(url.to_s)}>#{link_to body, url}</li>"
  end

  def class_active_controller(controller_name)
    if params[:controller] == controller_name
      return ' class="active"'.html_safe
    else
      return ' '
    end
  end

  # Generates a div (badge) to display the value of an attribute
  # Params:
  # +
  def attribute_badge(model, *args)
    attribute = args[0]
    label = attribute.capitalize
    as_percent = false
    if args.length > 1
      label = args[1]
      if args.length > 2
        as_percent = args[2]
      end
    end
    value = model.send(attribute)
    if as_percent
      value = (value*100).round.to_s + "%"
    end
    return raw "<div class=\"attribute attribute-#{attribute}\"><div class=\"attribute-value\">#{value}</div><div class=\"attribute-label\">#{label}</div></div>"
  end

end
