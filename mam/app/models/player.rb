class Player < ActiveRecord::Base
  belongs_to :user
  has_many :schools

  attr_accessible :avatar, :description, :name, :user_id

  has_attached_file :avatar, :styles => { :medium => "200x200>", :thumb => "50x50#" }
end
