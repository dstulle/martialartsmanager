require "names"
require "math"

class Buddy < ActiveRecord::Base
  include BuddyDefaultValues
  include BuddyAttributeValues

  belongs_to :school

  has_and_belongs_to_many :exmember_of_schools, :join_table => "buddies_exmembers_of_school", :class_name => "School"

  has_and_belongs_to_many :attending_courses, :join_table => "buddies_attending_courses", :class_name => 'Course'
  has_many :teaching_courses, :foreign_key => "teacher_id", :class_name => 'Course'

  attr_accessible :age, :constitution_exp, :dexterity_exp, :flexibility_exp, :injured, :intelligence_exp, :motivation, :name, :number_of_draws, :number_of_fights, :number_of_wins, :reaction_exp, :school_id, :skill_exp, :speed_exp, :strength_exp, :suitability, :waitlist

  before_save :default_values

  def number_of_losses
    self.number_of_fights - self.number_of_wins - self.number_of_draws
  end

  def waiting_for_school
    self.school if self.waitlist
  end

  def attending_school
    self.school if !self.waitlist    
  end

  def is_teacher?
    not self.teaching_courses.empty?
  end

  def is_student?
    not self.attending_courses.empty?
  end

end
