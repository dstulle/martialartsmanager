module BuddyAttributeValues
  
  def Buddy::max_attribute_value
    20
  end
  
  def Buddy::max_skill_value
    19
  end

  # 9 basic attributes
  
  def strength
    Buddy::exp2level(strength_exp)
  end
  
  def constitution
    Buddy::exp2level(constitution_exp)
  end
  
  def dexterity
    Buddy::exp2level(dexterity_exp)
  end
  
  def speed
    Buddy::exp2level(speed_exp)
  end
  
  def flexibility
    Buddy::exp2level(flexibility_exp)
  end
  
  def reaction
    Buddy::exp2level(reaction_exp)
  end
  
  def skill
    Buddy::exp2level(skill_exp,0,Buddy::max_skill_value)
  end
  
  def intelligence
    Buddy::exp2level(intelligence_exp)
  end

  # motivation ans suitability factors
  
  def motivationFactor
    (60 + (motivation.to_f/2))/100
  end
  
  def suitabilityFactor
    (60 + (suitability.to_f/2))/100
  end

  # teaching and learning
  
  def teachFactor
    learnTeach*motivationFactor
  end
  
  def learnFactor
    suitabilityFactor*motivationFactor
  end

  def learnTeach
   ((((skill*0.1 + intelligence*0.9)/(Buddy::max_attribute_value/2))-1)/3)+1
  end

  # fighting values

  def actions
   (constitution*0.1 + speed*0.8 + flexibility*0.1).round
  end

  def health
   (strength*0.5 + constitution*0.5).round
  end

  def defend
   ((constitution*0.4 + flexibility*0.1 + skill*0.5)/2).round
  end

  def hitpoints
   ((health*3 + defend*2)*2).round
  end

  def damage
   ((strength*0.5 + dexterity*0.1 + speed*0.1 + skill*0.2 + intelligence*0.1)/2).round
  end

  def criticalDamage
   (damage*2)
  end

  def toHit
   (dexterity*0.5 + flexibility*0.1 + reaction*0.2 + skill*0.2)/Buddy::max_attribute_value
  end

  def toCriticalHit
   (dexterity*0.4 + flexibility*0.4 + reaction*0.2)/(Buddy::max_attribute_value*2)
  end

  def defence
   (defend/2).round
  end

  def resistance
   (defend/2).round
  end

  def toDefend
   (speed*0.1 + flexibility*0.3 + reaction*0.6)/Buddy::max_attribute_value
  end

  # belt

  def belt_color
    Buddy::belt_color(skill)
  end

  private
  
  def Buddy::belt_color(skill)
    case skill
    when 0
      "no"
    when 1
      "white"
    when 2
      "yellow"
    when 3
      "orange"
    when 4
      "green"
    when 5
      "turquoise"
    when 6
      "blue"
    when 7
      "purple"
    when 8
      "red"
    when 9
      "brown"
    when 10
      "black-01"
    when 11
      "black-02"
    when 12
      "black-03"
    when 13
      "black-04"
    when 14
      "black-05"
    when 15
      "black-06"
    when 16
      "black-07"
    when 17
      "black-08"
    when 18
      "black-09"
    when 19
      "black-10"
    else
      "black"
    end
  end

  def Buddy::xp_grow_rate
    1.7
  end

  def Buddy::exp2level(exp, min_value=3, max_value=20)
    return 0 if exp.nil?
    Math.max(min_value,Math.min(max_value,(exp.to_f/10)**(1/Buddy::xp_grow_rate))).round
  end

end
