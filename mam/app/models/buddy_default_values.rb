module BuddyDefaultValues

  private
  def default_values
    self.name = nil if self.name == ""
    self.name ||= Names::getName
    self.age ||= (Math.abs((rand(10) + rand(10) + rand(10) + rand(10) + rand(10) + rand(10)) -26.5) + 5.5).to_i
    self.strength_exp ||= Buddy::get_exp_for_age(self.age,10+rand(5))
    self.constitution_exp ||= Buddy::get_exp_for_age(self.age,10+rand(5))
    self.dexterity_exp ||= Buddy::get_exp_for_age(self.age,10+rand(5))
    self.speed_exp ||= Buddy::get_exp_for_age(self.age,10+rand(5))
    self.flexibility_exp ||= Buddy::get_exp_for_age(self.age,10+rand(5))
    self.reaction_exp ||= Buddy::get_exp_for_age(self.age,10+rand(5))
    #self.skill_exp ||= Buddy::get_skill_exp_for_age(self.age,10+rand(5))  
    self.skill_exp ||= 0;
    self.intelligence_exp ||= Buddy::get_int_exp_for_age(self.age,10+rand(5))
    self.suitability ||= 0;
    self.motivation ||= (rand*100).to_i;
    self.injured ||= 0;
    self.number_of_fights ||= 0;
    self.number_of_wins ||= 0;
    self.number_of_draws ||= 0;
  end
  
  def Buddy::get_exp_for_age(age,learning_rate)
    initial_exp = 100
    initial_age = 6
    
    exp = initial_exp
    for i in initial_age..(age-1)
      exp = exp_one_week(exp,i,learning_rate)
    end
    
    exp
  end
  
  def Buddy::exp_one_day(exp,age,learning_rate=12.5,learning_factor=1)
    learn_age = 21
    forget_age = 11
    
    exp += ((learning_rate.to_f/2.5)*(learn_age.to_f/age).to_i)*learning_factor
    exp -= (exp*Math.max(0,(age-forget_age).to_f/3500)).to_i
    
    exp
  end
  
  def Buddy::exp_one_week(exp,age,learning_rate)
    7.times do
      exp = exp_one_day(exp,age,learning_rate)
    end
    
    exp
  end
  
  def Buddy::get_int_exp_for_age(age,learning_rate)
    initial_exp = 100
    initial_age = 6
    
    exp = initial_exp
    for i in initial_age..(age-1)
      exp = int_exp_one_week(exp,i,learning_rate)
    end
    
    exp
  end
  
  def Buddy::int_exp_one_day(exp,age,learning_rate,learning_factor=1)
    int_factor = 5.0
    
    exp += (((learning_rate.to_f/2.5)**(0.5) * int_factor**(1.5)/7.0).to_i)*learning_factor
    
    exp
  end
  
  def Buddy::int_exp_one_week(exp,age,learning_rate)
    7.times do
      exp = int_exp_one_day(exp,age,learning_rate)
    end
    
    exp
  end
  
  def Buddy::get_skill_exp_for_age(age,learning_rate)
    initial_exp = 0
    initial_age = 6
    
    exp = initial_exp
    for i in initial_age..(age-1)
      exp = skill_exp_one_week(exp,i,learning_rate)
    end
    
    exp
  end
  
  def Buddy::skill_exp_one_day(exp,age,learning_rate=12.5,learning_factor=1)
    int_factor = 5.0
    
    exp += (((learning_rate.to_f/2.5)**(0.5) * int_factor**(1.5)/5.0).to_i)*learning_factor
    
    exp
  end
  
  def Buddy::skill_exp_one_week(exp,age,learning_rate)
    7.times do
      exp = skill_exp_one_day(exp,age,learning_rate)
    end
    
    exp
  end
  
end
