class School < ActiveRecord::Base
  belongs_to :player

  has_many :courses

  has_many :buddies
  has_and_belongs_to_many :exmembers, :join_table => "buddies_exmembers_of_school", :class_name => "Buddy"

  attr_accessible :description, :logo, :name, :player_id

  has_attached_file :logo, :styles => { :medium => "200x200>", :thumb => "65x65#" }, :default_url =>"default_school_:style.png"

  def members
    self.buddies.select {|buddy| !buddy.waitlist}
  end

  def waitlist
    self.buddies.select {|buddy| buddy.waitlist}
  end

  def teachers
    self.members.find_all { |m| m.is_teacher?}
  end
  
  def students
    self.members.find_all { |m| m.is_student?}
  end

  def idle_members
    self.members - self.teachers - self.students
  end

end
