class Course < ActiveRecord::Base
  belongs_to :school

  belongs_to :teacher, :foreign_key => "teacher_id", :class_name => "Buddy"

  has_and_belongs_to_many :students, :join_table => "buddies_attending_courses", :class_name => 'Buddy'

  attr_accessible :name, :school_id, :teacher_id, :train_constitution, :train_dexterity, :train_flexibility, :train_intelligence, :train_reaction, :train_skill, :train_speed, :train_strength

  def teachers
    teacher ? [teacher] : []
  end

  def intensity
    (train_strength + train_constitution + train_dexterity + train_speed + train_flexibility + train_reaction + train_skill + train_intelligence).to_f / Course.max_training
  end

  def Course.max_training
    60
  end
  
  def risk
    Math.min(1,Math.max(0,(intensity - 1)*0.6))
  end

  def strength
   ((train_strength.to_f / intensity)*100)
  end
  
  def constitution
   ((train_constitution.to_f / intensity)*100)
  end
  
  def dexterity
   ((train_dexterity.to_f / intensity)*100)
  end
  
  def speed
   ((train_speed.to_f / intensity)*100)
  end
  
  def flexibility
   ((train_flexibility.to_f / intensity)*100)
  end
  
  def reaction
   ((train_reaction.to_f / intensity)*100)
  end
  
  def skill
   ((train_skill.to_f / intensity)*100)
  end
  
  def intelligence
   ((train_intelligence.to_f / intensity)*100)
  end

  def calculateAndSetSuitability
    suitabilities = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    active_students = 0
    unless (self.teacher == nil || self.students.empty? )
      if 0 == self.teacher.injured
        self.students.each do |s|
          if 0 == s.injured
            active_students += 1
            suitabilities[s.skill-2] += 0.2 if s.skill > 1
            suitabilities[s.skill-1] += 0.8 if s.skill > 0
            suitabilities[s.skill]   += 1.0
            suitabilities[s.skill+1] += 0.8 if s.skill < 19
            suitabilities[s.skill+2] += 0.2 if s.skill < 18
          end
        end
        for i in 0..suitabilities.size-1 do
          case i
            when teacher.skill
            suitabilities[i] = suitabilities[i]*0.8
            when teacher.skill+1
            suitabilities[i] = suitabilities[i]*0.2
            when (teacher.skill+2)..suitabilities.size
            suitabilities[i] = 0
          end
          suitabilities[i] = (suitabilities[i] / active_students)*2-1
        end
      end
    end
    self.students.each do |s|
      if 0 == s.injured
        s.suitability = Math.min(350 - s.age*5, (suitabilities[s.skill]*100).to_i)
      else
        s.suitability = 0
      end
      s.save
    end
    self.updateTeachersSuitability
  end
  
  def updateTeachersSuitability
    if self.teacher
      if 0 == self.teacher.injured
        motivation = 0
        active_students = 0
        unless self.students.empty?
          self.students.each do |s|
            if 0 == s.injured
              active_students += 1
              motivation += s.motivation
            end
          end
          motivation = motivation/active_students
        end
        self.teacher.suitability = Math.min(350 - teacher.age*5, motivation)
      else
        self.teacher.suitability = 0
      end
      self.teacher.save
    end
  end
  
  def effency
    1.12 - (students.size/50)
  end
  
  def addStudent(buddy)
    unless self.students.member?(buddy)
      self.removeTeacher(buddy)
      self.students += [buddy]
      self.save
      self.calculateAndSetSuitability
    end
  end
  
  def setTeacher(buddy)
    unless self.teacher == buddy
      self.removeStudent(buddy)
      if teacher
        self.teacher.suitability = 0 
        self.teacher.save
      end
      self.teacher = buddy
      self.save
      self.calculateAndSetSuitability
    end
  end
  
  def addBuddy(buddy)
    if self.teacher
      self.addStudent(buddy)
    else
      self.setTeacher(buddy)
    end
  end
  
  def removeStudent(buddy)
    if self.students.member?(buddy)
      self.students -= [buddy]
      self.save 
      buddy.suitability = 0
      buddy.save
      self.calculateAndSetSuitability
    end
  end
  
  def removeTeacher(buddy)
    if self.teacher == buddy
      self.teacher = nil
      self.save
      buddy.suitability = 0
      buddy.save
      self.calculateAndSetSuitability      
    end
  end
  
  def removeBuddy(buddy)
    self.removeTeacher(buddy)
    self.removeStudent(buddy)
    buddy.suitability = 0
    buddy.save
  end

end
