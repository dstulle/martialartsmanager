class SchoolsController < ApplicationController
  # GET /schools
  # GET /schools.json
  def index
    @schools = School.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @schools }
    end
  end

  # GET /schools/1
  # GET /schools/1.json
  def show
    @school = School.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @school }
    end
  end

  # GET /schools/new
  # GET /schools/new.json
  def new
    @school = School.new
    @players = Player.all

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @school }
    end
  end

  # GET /schools/1/edit
  def edit
    @school = School.find(params[:id])
    @players = Player.all
  end

  # POST /schools
  # POST /schools.json
  def create
    @school = School.new(params[:school])
    @players = Player.all

    age =  38 +rand(6)
    @school.buddies << Buddy.new("waitlist" => true, "age" => age, "skill_exp" => Math.max(500,Buddy::get_skill_exp_for_age(age/2,10+rand(5))))
    5.times do
      @school.buddies << Buddy.new("waitlist" => true)
    end

    respond_to do |format|
      if @school.save
        format.html { redirect_to @school, notice: 'School was successfully created.' }
        format.json { render json: @school, status: :created, location: @school }
      else
        format.html { render action: "new" }
        format.json { render json: @school.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /schools/1
  # PUT /schools/1.json
  def update
    @school = School.find(params[:id])
    @players = Player.all

    respond_to do |format|
      if @school.update_attributes(params[:school])
        format.html { redirect_to @school, notice: 'School was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @school.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /schools/1
  # DELETE /schools/1.json
  def destroy
    @school = School.find(params[:id])
    @school.destroy

    respond_to do |format|
      format.html { redirect_to schools_url }
      format.json { head :no_content }
    end
  end

  def add_members
    @school = School.find(params[:school])
    buddy_ids = params[:buddies] ? params[:buddies].keys : []
    buddy_ids.each do |buddy_id|
      buddy = Buddy.find(buddy_id)
      buddy.waitlist = false
      if "Dismiss" == params[:commit]
        buddy.school = nil
      end
      buddy.save

    end

    @school = School.find(params[:school])
    return_to = params[:return_to] || 'schools'
    redirect_to return_to
  end
    
  def dismiss_members
    @school = School.find(params[:school])
    buddy_ids = params[:buddies] ? params[:buddies].keys : []
    buddy_ids.each do |buddy_id|
      buddy = Buddy.find(buddy_id)
      buddy.school.exmembers << buddy
      buddy.school.save
      buddy.school = nil
      buddy.save
    end

    @school = School.find(params[:school])
    return_to = params[:return_to] || 'schools'
    redirect_to return_to
  end

end
